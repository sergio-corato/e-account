# -*- coding: utf-8 -*-
{
    'name': "Reminders and Agenda for Sale Order",
    'version': '8.0.1.0.0',
    'author': 'Sergio Corato',
    'license': 'LGPL-3',
    'category': 'Reminders and Agenda',
    'website': 'https://efatto.it',
    'depends': ['reminder_base', 'sale', 'sale_order_dates'],
    'data': [
        'views/sale_order.xml',
    ],
    'installable': True,
}
