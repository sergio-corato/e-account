.. image:: https://img.shields.io/badge/license-LGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/lgpl.html
   :alt: License: LGPL-3

====================
Mail Outbound Static
====================

This module brings Odoo outbound emails in to strict compliance with RFC-2822
by allowing for a statically configured From header, with the sender's e-mail
being appended into the proper Sender header instead.

Usage
=====

* Navigate to an Outbound Email Server
* Set the `Email From` option to an email address

.. image:: https://odoo-community.org/website/image/ir.attachment/5784_f2813bd/datas
   :alt: Try me on Runbot
   :target: https://runbot.odoo-community.org/runbot/205/10.0

Road Map
========

* Allow for domain-based whitelist that will not be manipulated

Bug Tracker
===========

Bugs are tracked on `GitHub Issues 
<https://github.com/OCA/social/issues>`_. In case of trouble, please
check there if your issue has already been reported. If you spotted it first, 
help us smash it by providing detailed and welcomed feedback.

Credits
=======

Images
------

* Odoo Community Association: `Icon <https://github.com/OCA/maintainer-tools/blob/master/template/module/static/description/icon.svg>`_.

Contributors
------------

* Dave Lasley <dave@laslabs.com>

Maintainer
----------

.. image:: https://odoo-community.org/logo.png
   :alt: Odoo Community Association
   :target: https://odoo-community.org

This module is maintained by the OCA.

OCA, or the Odoo Community Association, is a nonprofit organization whose
mission is to support the collaborative development of Odoo features and
promote its widespread use.

To contribute to this module, please visit https://odoo-community.org.
