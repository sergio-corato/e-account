# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* l10n_it_fatturapa_invoice_advance_auto
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-21 22:19+0000\n"
"PO-Revision-Date: 2019-09-21 22:19+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: l10n_it_fatturapa_invoice_advance_auto
#: field:account.invoice,is_advance_invoice:0
msgid "Advance invoice"
msgstr "Fattura di acconto"

#. module: l10n_it_fatturapa_invoice_advance_auto
#: model:ir.model,name:l10n_it_fatturapa_invoice_advance_auto.model_account_invoice
msgid "Invoice"
msgstr "Fattura"

#. module: l10n_it_fatturapa_invoice_advance_auto
#: model:ir.model,name:l10n_it_fatturapa_invoice_advance_auto.model_sale_advance_payment_inv
msgid "Sales Advance Payment Invoice"
msgstr "Anticipo su fattura di vendita"

#. module: l10n_it_fatturapa_invoice_advance_auto
#: view:account.invoice:l10n_it_fatturapa_invoice_advance_auto.view_invoice_form_advance
msgid "of advance"
msgstr "di acconto"

