# -*- coding: utf-8 -*-
# Copyright 2019 Sergio Corato
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).

{
    'name': 'ITA - Fattura elettronica - dati extra in righe',
    "summary": "Modulo per inserimento dati per connessioni con altri ERP",
    "version": "8.0.1.0.1",
    "development_status": "Beta",
    "category": "Hidden",
    'website': 'https://efatto.it',
    "author": "Sergio Corato",
    "maintainers": [],
    "license": "LGPL-3",
    "application": False,
    "installable": True,
    "auto_install": False,
    "depends": [
        "l10n_it_fatturapa_out",
        "sale_advance_invoice_progress",
    ],
}
