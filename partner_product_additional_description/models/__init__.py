# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from . import res_partner
from . import sale_order
from . import account_invoice
from . import package_preparation
from . import picking
from . import stock_move
